<?php include "functions.php"; ?>
<?php include "includes/header.php";?>

<section class="content">

    <aside class="col-xs-4">

        <?php Navigation();?>

    </aside>
    <!--SIDEBAR-->


    <article class="main-content col-xs-8">

        <?php  

// Step1: Make an if Statement with elseif and else to finally display string saying, I love PHP
$say_nothing = "";

if($say_nothing == "") {
	echo "I love PHP <br>";
}
elseif ($say_nothing == "Hello") {
	echo "Hello World! <br>";
}
else{
	echo "zzz";
}
// Step 2: Make a forloop  that displays 10 numbers
for ($i=0; $i < 10; $i++) { 
	echo $i;
	echo "<br>";
}
// Step 3 : Make a switch Statement that test againts one condition with 5 cases
$switch_variable = 20;
switch ($switch_variable) {
	case 10:
		echo "the value is $switch_variable";
		break;
	
	case 20:
		echo "the value is $switch_variable";
		break;
	
	case 30:
		echo "the value is $switch_variable";
		break;
	
	case 40:
		echo "the value is $switch_variable";
		break;
	
	case 50:
		echo "the value is $switch_variable";
		break;
	
	default:
		echo "Error! Invalid Value!";
		break;
}

?>






    </article>
    <!--MAIN CONTENT-->

    <?php include "includes/footer.php"; ?>