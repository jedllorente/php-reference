<?php include "functions.php"; ?>
<?php include "includes/header.php";?>

<section class="content">

    <aside class="col-xs-4">

        <?php Navigation();?>


    </aside>
    <!--SIDEBAR-->


    <article class="main-content col-xs-8">


        <?php  

		// Step1: Define a function and make it return a calculation of 2 numbers
		function addition(){
			$num1 = 20;
			$num2 = 30;

			$sum = $num1 + $num2;
			echo $sum . "<br>";
		}
		addition();
		// Step 2: Make a function that passes parameters and call it using parameter values
		$num1 = 10;
		$num2 = 20;

		function sum($a, $b) {
			$sum = $a + $b;
			echo $sum;
		}

		sum($num1, $num2);
	
		?>

    </article>
    <!--MAIN CONTENT-->


    <?php include "includes/footer.php"; ?>