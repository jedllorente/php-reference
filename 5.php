<?php include "functions.php"; ?>
<?php include "includes/header.php";?>
<section class="content">

    <aside class="col-xs-4">
        <?php Navigation();?>


    </aside>
    <!--SIDEBAR-->


    <article class="main-content col-xs-8">

        <?php 

		/**
		 * The function banker_roulette() creates an array of names, counts the number of names in the array,
		 * subtracts one from the count, creates a random number between 0 and the count, and then uses the
		 * random number to select a name from the array.
		 */
		function banker_roulette() {
			$names = array("Michael", "Jed", "Ian", "Wilson", "Rafael", "Raja");
			$count_names = count($names);
			$list_count = $count_names - 1;

			$random_count = rand(0, $list_count);
			$random_name = $names[$random_count];

			echo "$random_name is going to buy the meal today! <br>";
		}
		banker_roulette();


		/**
		 * The function forloop_test() creates an array of numbers, then uses a for loop to iterate through
		 * the array and print each number to the screen
		 */
		function forloop_test() {
			$numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

			for ($i=0; $i < count($numbers); $i++) { 
				echo $numbers[$i] . "<br>";
			}

			echo 'there are ' . count($numbers) . ' elements in the $numbers array. <br>';
		}
		forloop_test();

		// Step1: Use a pre-built math function here and echo it
		
		/**
		 * The function power() takes no arguments and returns the value of 2 to the power of 3
		 */
		$sample_value = 15;
		$sample_exponential_value = 10;
		 function power($a, $b) {
			$power = pow($a,$b);

			echo $power;
			echo "<br>";
		}
		power($sample_value, $sample_exponential_value);
		
		// Step 2:  Use a pre-built string function here and echo it
		
		/**
		 * The function count_characters() takes a string and counts the number of characters in it
		 */
		function count_characters(){
			$sample_string = "JedIanmichaelLlorente";
			$string_length = strlen($sample_string);

			echo 'the number of characters in "' . $sample_string .  '" is ' . $string_length . ' <br>';
		}
		count_characters();
		
		// Step 3:  Use a pre-built Array function here and echo it
		
		/**
		 * The function takes an array, adds two more elements to it, and then converts the array to a string
		 */
		function array_function() {
			$sample_array = array('Jed', 'Ian');
			array_push($sample_array, 'Michael', 'Llorente');
			$convert_to_string = implode(" ", $sample_array); 
			echo $convert_to_string . "<br>";
		}
		array_function();

		/**
		 * It takes an array, and returns a random element from that array
		 */
		function random_array() {
			$sample_array = array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
			$random_array = array_rand($sample_array);

			echo $random_array;

		}
		random_array();
		?>


    </article>
    <!--MAIN CONTENT-->
    <?php include "includes/footer.php"; ?>