<?php

$names = array('Jed', 'Ian', 'Michael', 'Raja');
$sample_password = "webadmin@1234!";

// !!! --- REMINDER! --- !!!
// isset() function checks whether if a variable is set. which will return true if the variable exists, and returns false if the variable does not exist.
// !!! --- REMINDER! --- !!!
$username = $_POST['username'];
$password = $_POST['password'];

/**
 * If the password is less than the sample password, then echo "The password isn't long enough. Try
 * again.". If the password is greater than the sample password, then echo "Multiple wrong entries of
 * login credentials. Please try again later.".
 */

/**
 * If the username is not in the array and the password is not the sample password, then increment the
 * login counter. If the username is in the array and the password is the sample password, then echo a
 * welcome message.
 */
function login_test() {

    global $username, $names, $password, $sample_password;

    if(!in_array($username, $names) && $password != $sample_password)
    {
        if(strlen($password) < strlen($sample_password))
        {
            echo "The password isn't long enough. Try again.";
        }
        else
        {
            echo "Error! Invalid login credentials. Try again.";
        }
    }
    else {
        echo 'Welcome ' . $username . '! Your password is ' . $password . '.';
    }

}